import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button
} from 'react-native';


export default class Login extends Component{
  constructor(props){
    super(props);
    this.state = {
      emailInput: null,
      passwordInput: null,
      message: null
    }
  }

  static navigationOptions = {
    title: 'Login',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  handleLogin = () => {
    let proceed = false;
    let proceedC = false;
    let proceedD = false;
    fetch('https://nm5wckusxi.execute-api.us-east-1.amazonaws.com/dev/users/login', {
      method: 'POST',
      body: JSON.stringify({
        password: this.state.passwordInput,
        email: this.state.emailInput,
      }),
    })
    .then((response) => response.json())
    .then((response) => {
      console.log(response)
      if(response.id) proceed = true;
      if (response.employee_type=="Cliente") proceedC = true;
      if (response.employee_type!="Cliente") proceedD = true;
      
    })
    .then(() => {
      console.log(proceed)
      if (proceed && proceedC) this.props.navigation.navigate('CMap');
      if (proceed && proceedD) this.props.navigation.navigate('Map');
      if (!proceed) alert('Wrong password or email.')
    })
    

  }
  

  handleOnChangeUsername = (text) => {
    this.setState({
      emailInput: text
    })
  }

  handleOnChangePassword = (text) => {
    this.setState({
      passwordInput: text
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Email:</Text>
        <TextInput
          underlineColorAndroid = 'transparent'
          style={styles.textInput}
          onChangeText = {this.handleOnChangeUsername}
        />
        <Text style={styles.text}>Password:</Text>
        <TextInput
          secureTextEntry={true}
          underlineColorAndroid = 'transparent'
          style={styles.textInput}
          onChangeText = {this.handleOnChangePassword}
        />
        <Button
          title="Go to Map"
          onPress={() => this.handleLogin()}
          // onPress={() => this.props.navigation.navigate('Map')}
          color="#f4511e"
        />
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  textInput: {
    height: 50,
    width: 200,
    borderWidth: 4,
    borderRadius: 20,
    borderColor: '#f4511e',
    marginBottom: 10,
    marginHorizontal: 10,
    paddingHorizontal: 10
  },
  text: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 18
  }
});