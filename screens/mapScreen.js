import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import MapView from 'react-native-maps';
import Marker from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const locations = [
  {
    _id: 0,
    latitude: 18.4623115,
    longitude: -69.92664030000003
  },
  {
    _id: 0,
    latitude: 18.4623115,
    longitude: -69.9098968
  },
  {
    _id: 0,
    latitude: 18.4723115,
    longitude: -69.8898968
  }
]


export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      region: {
        latitude: 0, 
        longitude: 0, 
        latitudeDelta: 0.1,
        longitudeDelta: 0.1
      },
      isLoading: true,
      dataSource: [],
      currentLat: 18.4623115,
      currentLong: -69.92664030000003,
      error: null
    }
  };

  static navigationOptions = {
    title: 'Route',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  componentDidMount(){//CAMBIAR POR ID DINAMICO
    this.getPosition();
    this.fetchRoutes();
    this.setInitialRegion();
    
  }

  onRegionChange = (newRegion) => {
    this.setState({
      region: newRegion
    })
  }

  setInitialRegion = () => {
    initialRegion = {
      latitude: locations[0].latitude, //CAMBIAR POR DATASOURCE
      longitude: locations[0].longitude, //CAMBIAR POR DATASOURCE
      latitudeDelta: 0.1,
      longitudeDelta: 0.1
    }
    this.setState({
      region: initialRegion
    })
  }

  getPosition = () => {
    
    navigator.geolocation.getCurrentPosition(
      (position) => {
        
        console.log(position);
        this.setState({
          currentLat: position.coords.latitude,
          currentLong: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    );
  };

  fetchRoutes = () => {
    console.log("holaaaaa"); 
    fetch('https://nm5wckusxi.execute-api.us-east-1.amazonaws.com/dev/route/8969cac0-91b5-11e8-9c24-a999184df9b9')
      .then((response) => response.json())
      .then((responseJson) => {
       
        this.setState({
          isLoading: false,
          dataSource: responseJson.customer,
        }, function(){
        });
        console.log(this.state.dataSource);
      })
      .catch((error) =>{
        console.error(error);
      });
  }
  
  render() {
    let origin = {latitude: this.state.currentLat, longitude: this.state.currentLong}; //CAMBIAR POR DATASOURCE
    const destination = locations[locations.length-1]; //CAMBIAR POR DATASOURCE
    const GOOGLE_MAPS_APIKEY = 'AIzaSyBZ_WZKMlALrSkDS9SNOEQFA60qYBJQBw8';

    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={ this.state.region }
        >
          {this.state.dataSource.map(location => { //CAMBIAR POR DATASOURCE
            const [longitude, latitude] = [location.longitude*1, location.latitude*1];
            return(
              <MapView.Marker
                coordinate = {{longitude, latitude}}
              >
              </MapView.Marker>
            );
          })}
          <MapView.Marker //LLEGADA
            coordinate = {destination}
            pinColor = "blue"
          >
          </MapView.Marker>
          <MapViewDirections
            origin={destination}
            destination={destination}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor="hotpink"
            waypoints={this.state.dataSource} //FIX CAMBIAR POR DATASOURCE
          />
        </MapView>
        <Button
          title="Go to Complaints"
          onPress={() => this.props.navigation.navigate('Complain')}
          color="#f4511e"
        />
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});