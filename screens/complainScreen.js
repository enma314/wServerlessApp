import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button
} from 'react-native';


export default class Login extends Component{
  constructor(props){
    super(props);
    this.state = {
      complainInput: null,
      customerInput: null
    }
  }

  static navigationOptions = {
    title: 'Complaint',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  handleOnChangeComplain = (text) => {
    this.setState({
      complainInput: text
    })
  }

  handleOnChangeCustomer = (text) => {
    this.setState({
      customerInput: text
    })
  }

  handleComplainPost = () => {
    if(this.state.complainInput==null || this.state.customerInput==null) {
      alert('Fill all fields please.');
    }

    else {
      fetch('https://nm5wckusxi.execute-api.us-east-1.amazonaws.com/dev/complaints', {
        method: 'POST',
        body: JSON.stringify({
          customer: this.state.customerInput,
          problem: this.state.complainInput,
        }),
      })
      .then((response) => response.json())
      .then((response) => {
        console.log(response)
        if (response.id) alert('Complaint added.');
        this.textInput.clear();
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Customer:</Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={styles.textInputCus}
          onChangeText = {this.handleOnChangeCustomer}
          ref={input => { this.textInput = input }}
        />
        <Text style={styles.text}>Problem:</Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={styles.textInput}
          onChangeText = {this.handleOnChangeComplain}
          multiline={ true }
          ref={input => { this.textInput = input }}
        />
        <Button
          title="Send Complaint"
          onPress={() => this.handleComplainPost()}
          color="#f4511e"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#fff69b',
    backgroundColor: 'white',
  },
  text: {
    fontWeight: 'bold',
    //color: '#f4511e',
    color: 'black',
    fontSize: 18,
    margin: 10
  },
  textInput: {
    height: '30%',
    width: 200,
    borderWidth: 4,
    borderRadius: 20,
    borderColor: '#f4511e',
    marginBottom: 10,
    marginHorizontal: 10,
    paddingHorizontal: 10
  },
  textInputCus: {
    height: 50,
    width: 200,
    borderWidth: 4,
    borderRadius: 20,
    borderColor: '#f4511e',
    marginBottom: 10,
    marginHorizontal: 10,
    paddingHorizontal: 10
  },
});