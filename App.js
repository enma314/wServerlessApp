/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import MapView from 'react-native-maps';
import Marker from 'react-native-maps';
import { createStackNavigator } from 'react-navigation';
import CMapScreen from './screens/mapScreen';
import MapScreen from './screens/mapScreenDriver';
import LoginScreen from './screens/loginScreen';
import ComplainScreen from './screens/complainScreen';

const RootStack = createStackNavigator(
  {
    Login: LoginScreen,
    Map: MapScreen,
    CMap: CMapScreen,
    Complain: ComplainScreen
  },
  {
    initialRouteName: 'Login',
  },
  {  
  headerMode: 'screen'
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

